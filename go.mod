module gitlab.com/tslocum/cview

go 1.12

require (
	github.com/gdamore/tcell/v2 v2.0.0
	github.com/lucasb-eyer/go-colorful v1.0.3
	github.com/mattn/go-runewidth v0.0.9
	github.com/rivo/uniseg v0.1.0
	gitlab.com/tslocum/cbind v0.1.3
	golang.org/x/sys v0.0.0-20201016160150-f659759dc4ca // indirect
)
